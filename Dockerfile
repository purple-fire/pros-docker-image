FROM python:3-stretch
MAINTAINER https://gitlab.com/purple-fire/pros-docker-image

# Install PROS-CLI
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install "https://github.com/purduesigbots/pros-cli/releases/download/3.1.4/pros_cli_v5-3.1.4-py3-none-any.whl"

# Install ARM GCC toolchain
RUN curl -L "https://developer.arm.com/-/media/Files/downloads/gnu-rm/8-2019q3/RC1.1/gcc-arm-none-eabi-8-2019-q3-update-linux.tar.bz2" > gcc-arm-none-eabi.tar.bz2
RUN tar xf gcc-arm-none-eabi.tar.bz2
RUN mv gcc-arm-none-eabi-8-2019-q3-update /opt/gcc-arm-none-eabi

ENV PATH=/opt/gcc-arm-none-eabi/bin:$PATH
